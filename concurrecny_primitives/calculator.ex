defmodule Calculator do

  def start do
    spawn(fn -> loop(0) end)
  end

  def value(server_pid) do
    # This execution is happening in seprate thread same as client process since client has started this so to get
    # result need to send msg to server pid to return the value
    send(server_pid, {:value, self()})
    receive do # receive block waiting on client process
      {:response, value} ->
        value
      end
  end

  def add(server_pid, value), do: send(server_pid, {:add, value}) # Client Process sending msg to server_pid to process teh msg
  def sub(server_pid, value), do: send(server_pid, {:sub, value}) # Client Process sending msg to server_pid to process teh msg
  def mul(server_pid, value), do: send(server_pid, {:mul, value}) # Client Process sending msg to server_pid to process teh msg
  def div(server_pid, value), do: send(server_pid, {:div, value}) # Client Process sending msg to server_pid to process teh msg

  defp loop(state) do # This excution is happening on the Server Process ..
    new_value =
      receive do
        message ->
          process_message(state, message)
      end
      loop(new_value)
  end


  defp process_message(current_value, {:value, caller}) do
    send(caller, {:response, current_value})
    current_value
  end
  defp process_message(current_value, {:add, value}) do
    current_value + value
  end
  defp process_message(current_value, {:sub, value}) do
    current_value - value
  end
  defp process_message(current_value, {:mul, value}) do
    current_value * value
  end
  defp process_message(current_value, {:div, value}) do
    current_value / value
  end
  defp process_message(current_value, invalid_request) do
    IO.puts("invalid request #{inspect invalid_request}")
    current_value
  end
end
