defmodule KeyStore do
  alias ServerProcesses

  def init() do  # called by ServerProcess to get initial State.
    %{}
  end

  def handle_call({:put,key,value},state) do # # called by ServerProcess to handle put message
    {:ok, Map.put(state, key, value)}
  end

  def handle_call({:get, key},state) do   # called by ServerProcesses to handle get Messages
    {Map.get(state, key), state}
  end

  def start() do # exposed to client to start the process
    ServerProcesses.start(KeyStore)
  end

  def put(pid,key,value) do  # exposed to client to put a key, value
    ServerProcesses.call(pid, {:put, key, value})
  end

  def get(pid,key) do  # exposed to client to get a key stored
    ServerProcesses.call(pid, {:get, key})
  end
end

# c("server_process.ex")
# c("key_store.ex")
# server_pid = KeyStore.start()
# KeyStore.put(server_pid, key, value)
# KeyStore.get(server_pid,key)



# Important Notes

# Clients can now use start/0, put/3, and get/2 to manipulate the key-value store.
# These functions are informally called interface functions. Clients use the interface functions of
# KeyValueStore to start and interact with the process.
# 
# In contrast, init/0 and handle_call/2 are callback functions used internally
# by the generic code. Note that interface functions run in client processes,
# whereas callback functions are always invoked in the server process.
