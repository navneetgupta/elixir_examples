defmodule ToDoUpdated do
  def start() do
    ServerProcessesAsynnc.start(ToDoUpdated)
  end

  def add(server_pid, new_entry) do
    ServerProcessesAsynnc.cast(server_pid, {:add_entry, new_entry})
  end

  def get(server_pid,date) do
    ServerProcessesAsynnc.call(server_pid,{:entries, date})
  end

  def init() do
    ToDoList.new();
  end

  def handle_cast({:add_entry, new_entry}, state) do
     Todolist.add_entry(state, new_entry)
  end

  def handle_msg({:entries, date}, state) do
     {Todolist.enteries(state, date),state}
  end


  #
  # def loop(state) do
  #   new_todo_list =
  #     receive do
  #       message -> process_message(state, message)
  #     end
  #   loop(new_todo_list)
  # end
  #
  # def add_entry(todo_server, new_entry) do
  #   send(todo_server, {:add_entry, new_entry})
  # end
  #
  # def entries(todo_server, date) do
  #   send(todo_server, {:entries, self(), date})
  #   receive do
  #     {:todo_entries, entries} -> entries
  #   after
  #     3000 -> {:error, :timeout}
  #   end
  # end
  #
  # def process_message(state, {:add_entry, new_entry}) do
  #   Todolist.add_entry(state, new_entry)
  # end
  #
  # def process_message(state, {:entries, caller, date}) do
  #   send(caller, {:todo_entries, Todolist.enteries(state, date)})
  #   state
  # end
end

defmodule ServerProcessesAsynnc do
  def start(callback_module) do
    spawn(fn ->
      initial_state = callback_module.init()
      loop(callback_module,initial_state)
    end)
  end

  defp loop(callback_module, state) do
    receive do
      {:call, request, caller} ->
        {response,new_state} =
          callback_module.handle_msg(request,state)
        send(caller,{:response, response})
        loop(callback_module,new_state)
      {:cast, request} ->
        new_state =
          callback_module.handle_cast(
            request,
            state
          )
        loop(callback_module,new_state)
    end
  end

  def call(server_pid, request) do
    send(server_pid, {:call, request, self()})
    receive do
      {:response,response} ->
        response
    end
  end

  def cast(server_pid, request) do
    send(server_pid, {:cast, request})
  end

end

defmodule Todolist do
  defstruct auto_id: 1, entries: %{}

  # def new(), do: %TODOList{}

  def new(entries \\ [] ) do
    Enum.reduce(entries, %Todolist{}, &add_entry(&2, &1))
  end

  def add_entry(todo_list, entry) do
     entry = Map.put(entry, :id, todo_list.auto_id)
     new_entries = Map.put(todo_list.entries, todo_list.auto_id, entry)
     %Todolist{ todo_list |
       entries: new_entries,
       auto_id: todo_list.auto_id + 1
     }
  end

  def enteries(todo_list, date) do
    todo_list.entries
    |> Stream.filter(fn {_, entry} -> entry.date == date end)
    |> Enum.map(fn {_, entry} -> entry end)
  end

  def update_entry(todo_list, entry_id, updater_fn) do
    case Map.fetch(todo_list.entries, entry_id) do
      :error ->
        todo_list
      {:ok, old_entry}  ->
         old_entry_id = old_entry.id
         new_entry= %{id: ^old_entry_id} = updater_fn.(old_entry)
         new_entries = Map.put(todo_list.entries, old_entry_id , new_entry)
         %Todolist{todo_list | entries: new_entries}
    end
  end

  def update_entry(todo_list, %{} = new_entry) do
    update_entry(todo_list, new_entry.id, fn _ -> new_entry end)
  end

  def delete_entry(todo_list, entry_id) do
    %Todolist{todo_list | entries: Map.delete(todo_list.entries, entry_id)}
  end
end
