# generic code
defmodule ServerProcesses do
  def start(callback_module) do
    spawn(fn ->
      initial_state = callback_module.init()
      loop(callback_module,initial_state)
    end)
  end

  defp loop(callback_module, state) do
    receive do
      {request, caller} ->
        {response,new_state} =
          callback_module.handle_call(request,state)
        send(caller,{:response, response})
        loop(callback_module,new_state)
    end
  end

# function to issue requests to the server process.
  def call(server_pid, request) do
    send(server_pid, {request, self()})
    receive do
      {:response,response} ->
        response
    end
  end

end
