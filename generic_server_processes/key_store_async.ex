defmodule KeyStoreAsync do
  alias ServerProcessesAsynnc

  def init() do
    %{}
  end

  # def handle_msg({:put,key,value},state) do
  #   {:ok, Map.put(state, key, value)}
  # end
  #
  def handle_call({:get, key},state) do
    {Map.get(state, key), state}
  end

  def start() do
    ServerProcessesAsynnc.start(KeyStoreAsync)
  end

  def put(pid,key,value) do
    ServerProcessesAsynnc.cast(pid, {:put, key, value})
  end

  def get(pid,key) do
    ServerProcessesAsynnc.call(pid, {:get, key})
  end

  def handle_cast({:put, key, value},state) do
    Map.put(state, key, value)
  end
end
