defmodule Fraction do

  defstruct [
    :a,
    :b
  ]

  def new(a, b) do
    %Fraction{a: a, b: b}
  end

  def add(%Fraction{a: a, b: b} ,%Fraction{a: a1, b: b1}) do
    %Fraction{a: a*b1+a1*b, b: b*b1}
  end

  def one_half(%Fraction{a: a, b: b}) do
    %Fraction{a: a, b: b*2}
  end

  def one_quarter(%Fraction{a: a, b: b}) do
    %Fraction{a: a, b: b*4}
  end

  def value(%Fraction{a: a, b: b}) do
    a/b
  end

  def value1(fraction) do
    fraction.a/fraction.b
  end

  # The  value1() code is arguably clearer, but it will run slightly more slowly than the previous
  # case where you read all fields value() in a match.

end

#  iex(1)> one_half = %Fraction{a: 1, b: 2}
