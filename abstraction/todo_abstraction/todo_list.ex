defmodule TodoAbstraction.TodoList do

  alias TodoAbstraction.MultiDict

  def new(), do: MultiDict.new()

  def add_entry(todo_list, date, title) do
     MultiDict.add(todo_list,date,title)
  end

  def get_entry(todo_list,date) do
    MultiDict.get(todo_list,date)
  end

  def entry_due_today(todo_list) do
    MultiDict.get(todo_list,Date.utc_today())
  end
end
