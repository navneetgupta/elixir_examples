defmodule TODOList do
  defstruct auto_id: 1, entries: %{}

  # def new(), do: %TODOList{}

  def new(entries \\ [] ) do
    Enum.reduce(entries, %TODOList{}, &add_entry(&2, &1)) # fn x, acc -> add_entry(acc, x) end
  end

  def add_entry(todo_list, entry) do
     entry = Map.put(entry, :id, todo_list.auto_id)
     new_entries = Map.put(todo_list.entries, todo_list.auto_id, entry)
     %TODOList{ todo_list |
       entries: new_entries,
       auto_id: todo_list.auto_id + 1
     }
  end

  def enteries(todo_list, date) do
    todo_list.entries
    |> Stream.filter(fn {_, entry} -> entry.date == date end)
    |> Enum.map(fn {_, entry} -> entry end)
  end

  def update_entry(todo_list, entry_id, updater_fn) do
    case Map.fetch(todo_list.entries, entry_id) do
      :error ->
        todo_list
      {:ok, old_entry}  ->
         old_entry_id = old_entry.id
         new_entry= %{id: ^old_entry_id} = updater_fn.(old_entry)
         new_entries = Map.put(todo_list.entries, old_entry_id , new_entry)
         %TODOList{todo_list | entries: new_entries}
    end
  end

  def update_entry(todo_list, %{} = new_entry) do
    update_entry(todo_list, new_entry.id, fn _ -> new_entry end)
  end

  def delete_entry(todo_list, entry_id) do
    %TODOList{todo_list | entries: Map.delete(todo_list.entries, entry_id)}
  end

end

defmodule TodoList.CsvImporter do
  def import(filename) do
    filename
    |> read_lines
    |> create_entries
    |> TODOList.new
  end

  def read_lines(filename) do
    file_name
    |> File.stream!()
    |> Stream.map(&String.replace(&1, "\n", ""))
  end

  def create_entries(lines) do
    lines
    |> Stream.map(&extract_fields/1)
    |> Stream.map(&create_entry/1)
  end

  def extract_fields(line) do
    line
    |> String.split(",")
    |> convert_date
  end

  def convert_date([date_string,title]) do
    [year, month,date] = date_string
    |> String.split("/")
    |> Enum.map(&String.to_integer/1)

    {:ok, date} = Date.new(year, month, day)
    {date, title}
  end

  def create_entry({date,title}) do
    %{date: date, title: title}
  end
end
